package test;

import static org.junit.Assert.*;
import moteur.MoteurMetronome;
import moteur.MoteurMetronomeImpl;

import org.junit.Before;
import org.junit.Test;

import controller.Controleur;
import controller.ControleurImpl;

public class TestMoteurMetronomeImpl {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testMoteurMetronomeImpl() {
		Controleur controleur = new ControleurImpl();
		MoteurMetronome moteurMetronome = new MoteurMetronomeImpl(controleur);
		
		String messageContext = "moteurMetronomeImpl() constructeur";
		
		assertNotNull(messageContext,moteurMetronome);
		
		boolean etatMarche = moteurMetronome.getEtatMarche();
		assertFalse(messageContext, etatMarche);
	}

	@Test
	public void testGetTempo() {
		Controleur controleur = new ControleurImpl();
		MoteurMetronome moteurMetronome = new MoteurMetronomeImpl(controleur);
		float expectedTempo = 120;
		float tempo = moteurMetronome.getTempo();
		
		if(expectedTempo != tempo) {
			fail("Init Tempo devait �tre de "+expectedTempo);
		}
		
		expectedTempo = 200;
		moteurMetronome.setTempo(expectedTempo);
		tempo = moteurMetronome.getTempo();
		
		if(expectedTempo != tempo) {
			fail("Tempo attentu devait �tre de "+expectedTempo);
		}
		
	}

	@Test
	public void testSetTempo() {
		Controleur controleur = new ControleurImpl();
		MoteurMetronome moteurMetronome = new MoteurMetronomeImpl(controleur);
		
		float expectedTempo = 170;
		moteurMetronome.setTempo(expectedTempo);
		float tempo = moteurMetronome.getTempo();
		
		if(expectedTempo != tempo) {
			fail("Init Tempo devait �tre de "+expectedTempo);
		}
		
		expectedTempo = -5;
		moteurMetronome.setTempo(expectedTempo);
		tempo = moteurMetronome.getTempo();
		
		if(expectedTempo == tempo) {
			fail("Exepected tempo inferieur � 0");
		}
	}

	@Test
	public void testGetEtatMarche() {
		Controleur controleur = new ControleurImpl();
		MoteurMetronome moteurMetronome = new MoteurMetronomeImpl(controleur);
		
		assertFalse("moteurMetronome init", moteurMetronome.getEtatMarche());
		moteurMetronome.setEtatMarche(true);
		assertTrue("moteurMetronome etat marche set true", moteurMetronome.getEtatMarche());
		
	}

	@Test
	public void testSetEtatMarche() {
		Controleur controleur = new ControleurImpl();
		MoteurMetronome moteurMetronome = new MoteurMetronomeImpl(controleur);
		
		assertFalse("moteurMetronome init", moteurMetronome.getEtatMarche());
		moteurMetronome.setEtatMarche(true);
		assertTrue("moteurMetronome etat marche set true", moteurMetronome.getEtatMarche());
	}

	@Test
	public void testSetNbTemps() {
		Controleur controleur = new ControleurImpl();
		MoteurMetronome moteurMetronome = new MoteurMetronomeImpl(controleur);
		
		int expectedNBTps = 4;
		moteurMetronome.setNbTemps(expectedNBTps);
		int nbTps = moteurMetronome.getNbTemps();
		
		if(expectedNBTps != nbTps) {
			fail("Init Tempo devait �tre de "+expectedNBTps);
		}
		
		expectedNBTps = -5;
		moteurMetronome.setNbTemps(expectedNBTps);
		nbTps = moteurMetronome.getNbTemps();
		
		if(expectedNBTps == nbTps) {
			fail("Exepected tempo inferieur � 0");
		}
		
		expectedNBTps = 10;
		moteurMetronome.setNbTemps(expectedNBTps);
		nbTps = moteurMetronome.getNbTemps();
		
		if(expectedNBTps == nbTps) {
			fail("Exepected tempo superieur � 7");
		}
	}

	@Test
	public void testGetNbTemps() {
		Controleur controleur = new ControleurImpl();
		MoteurMetronome moteurMetronome = new MoteurMetronomeImpl(controleur);
		int expectedNBTps = 4;
		int nbTps = moteurMetronome.getNbTemps();
		
		if(expectedNBTps != nbTps) {
			fail("Init nb tps devait �tre de "+expectedNBTps);
		}
		
		expectedNBTps = 2;
		moteurMetronome.setNbTemps(expectedNBTps);
		nbTps = moteurMetronome.getNbTemps();
		
		if(expectedNBTps != nbTps) {
			fail("Tempo attentu devait �tre de "+expectedNBTps);
		}	
	}

}
