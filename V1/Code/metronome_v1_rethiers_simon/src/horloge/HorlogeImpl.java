package horloge;

import java.util.Timer;

import commandes.Commande;
/**
 * Classe concrete de horloge
 * Utilise des elements java (Timer, TimerTask)
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public class HorlogeImpl implements Horloge {

	private Timer timerPeriodique; 
	public static final long DELAI_PERIODIQUE = 0;
	
	@Override
	public void activerPeriodiquement(Commande cmd, float periodeEnSecondes) {
		timerPeriodique = new Timer();
		TachePeriodique tachePeriodique = new TachePeriodique(cmd);
		long periodeEnMiliSecondes = this.tempsEnMilliSecondes(periodeEnSecondes);
		
		timerPeriodique.schedule(tachePeriodique, DELAI_PERIODIQUE, periodeEnMiliSecondes);
	}

	@Override
	public void activerApresDelai(Commande cmd, float delaiEnSecondes) {
		Timer timerSpecifique = new Timer();
		long delaiEnMiliSecondes = this.tempsEnMilliSecondes(delaiEnSecondes);
		TachePeriodiqueDelai tacheAvecDelai = new TachePeriodiqueDelai(cmd, timerSpecifique);
		timerSpecifique.schedule(tacheAvecDelai, delaiEnMiliSecondes);
	}

	@Override
	public void desactiver() {
		timerPeriodique.cancel();
	}
	
	/**
	 * Renvoi le temps "tempsEnSeconde" en milli secondes
	 * @param tempsEnSeconde
	 * @return
	 */
	private long tempsEnMilliSecondes(float tempsEnSeconde) {
		return (long) (1000 * tempsEnSeconde);
	}

}
