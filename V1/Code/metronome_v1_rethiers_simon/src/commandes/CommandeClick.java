package commandes;

import moteur.MoteurMetronome;
/**
 * 
 * @author Jean-Francois Rethiers & Flavien Simon
 * Classe concrete marquant le click appelee par l'Horloge
 * transmet le click au moteur metronome
 */
public class CommandeClick implements Commande {
	
	private MoteurMetronome moteurM;
	
	@Override
	public void execute() {
		moteurM.click();
	}
/**
 * Constructeur
 * @param moteurM classe concrete implementant l'interface MoteurMetronome
 */
	public CommandeClick(MoteurMetronome moteurM) {
		super();
		this.moteurM = moteurM;
	}
}
