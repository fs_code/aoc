package controller;

import ihm.IHM;
import ihm.IHM.LED;
import ihm.IHMImpl;
import ihm.IHM.TOUCHE;

import java.util.Observable;

import commandes.Commande;
import commandes.CommandeClick;
import moteur.MoteurMetronome;
import moteur.MoteurMetronomeImpl;

/**
 * Classe implementant l'interface Controleur
 * possède une reference vers 
 * un MoteurMetronome
 * une Commande
 * une IHM
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public class ControleurImpl implements Controleur {

	private MoteurMetronome moteurMetronome;
	private Commande commandeClick;
	private IHM ihm;

	/**
	 * Constructeur
	 * initialise le moteur l'IHM et la commande click
	 */
	public ControleurImpl() {
		super();
		moteurMetronome = new MoteurMetronomeImpl(this);
		ihm = new IHMImpl(this);
		Thread t = new Thread((IHMImpl)ihm);
		t.start();
		commandeClick = new CommandeClick(moteurMetronome);
	}

	@Override
	public void update(Observable o, Object arg) {
		
		if(o instanceof IHM){	
			TOUCHE touchePresse = ihm.getToucheEnfoncee();

			switch (touchePresse) {
			case DEMARRER:
				this.demarrer();
				break;

			case ARRETER:
				this.arreter();
				break;

			case INC:
				this.incrementerNBTempsMesure();
				break;

			case DEC:
				this.decrementerNSTempsMesure();
				break;

			case AUCUNE: //cas où c'est un changement de tempo
				int newTempo = ihm.getTempoIHM();
				moteurMetronome.setTempo(newTempo);
				break;

			default:
				break;
			}

		}

		if(o instanceof MoteurMetronome){	
			if(moteurMetronome.getEtatMarche() == true){
				//Changement de tempo
				ihm.getHorloge().desactiver();
				ihm.getHorloge().activerPeriodiquement(commandeClick, this.tempoEnSeconde());
			}
		}

	}

	@Override
	public void demarrer() {
		if(moteurMetronome.getEtatMarche() == true) {
			return;
		}

		moteurMetronome.setEtatMarche(true);
		ihm.getHorloge().activerPeriodiquement(commandeClick, this.tempoEnSeconde());
	}
	/**
	 * Donne la valeur du tempo actuel en seconde
	 * @return
	 */
	private float tempoEnSeconde() {
		float tempo = moteurMetronome.getTempo();
		float nbSecPerMinute = 60;
		return nbSecPerMinute/tempo;
	}

	@Override
	public void arreter() {
		if(moteurMetronome.getEtatMarche() == false)
			return;

		moteurMetronome.setEtatMarche(false);
		ihm.getHorloge().desactiver();
	}

	@Override
	public void marquerTemps() {
		ihm.allumerLed(LED.LED_TEMPS);
	}

	@Override
	public void marquerMesure() {
		ihm.allumerLed(LED.LED_MESURE);
	}

	@Override
	public void incrementerNBTempsMesure() {
		int nbTempsMesure = moteurMetronome.getNbTemps();
		if(nbTempsMesure < 7) {
			nbTempsMesure++;
			moteurMetronome.setNbTemps(nbTempsMesure);
		}
		System.out.println("nb temps Mesure = "+nbTempsMesure);
	}

	@Override
	public void decrementerNSTempsMesure() {
		int nbTempsMesure = moteurMetronome.getNbTemps();
		if(nbTempsMesure > 2) {
			nbTempsMesure--;
			moteurMetronome.setNbTemps(nbTempsMesure);
		}
		System.out.println("nb temps Mesure = "+nbTempsMesure);
	}

}
