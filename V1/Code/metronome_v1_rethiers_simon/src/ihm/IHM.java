package ihm;

import horloge.Horloge;

/**
 * Interface IHM
 * @author Jean-Francois Rethiers & Flavien Simon
 * Definie les methodes a implementer pour l'interface
 */
public interface IHM {
	
	public enum TOUCHE {DEMARRER, ARRETER, INC, DEC , AUCUNE};
	public enum LED {LED_TEMPS , LED_MESURE}

	/**
	 * Allume la LED voulue 
	 * @param led deux valeurs possibles
	 * LED_TEMPS d�signe la led qui s'allume a chaque temps
	 * LED_MESURE d�signe la led qui s'allume a chaque mesure
	 */
	public void allumerLed(LED led);
	
	/**
	 * Retourne la derniere touche enfoncee
	 * @return 5 valeurs possibles selon l'enum TOUCHE
	 * DEMARRER d�signe la touche pour d�marrer
	 * ARRETER d�signe la touche pour arreter
	 * INC d�signe la touche pour augmenter le nombre de temps par mesure
	 * DEC d�signe la touche pour diminuer le nombre de temps par mesure
	 * AUCUNE signifie qu'aucune touche du clavier n'est enfonc�e
	 */
	public TOUCHE getToucheEnfoncee();
	
	/**
	 * Donne la valeur du tempo de l'IHM
	 * @return
	 */
	public int getTempoIHM();
	
	/**
	 * Renvoie l'horloge
	 * @return
	 */
	public Horloge getHorloge();
}
