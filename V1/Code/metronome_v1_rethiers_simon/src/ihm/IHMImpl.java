package ihm;

import horloge.Horloge;
import horloge.HorlogeImpl;

import java.util.Observable;
import java.util.Scanner;

import controller.Controleur;

/**
 * Classe implementant l'interface IHM
 * Methode utilisee : ligne de commandes
 * Liste de commandes : 
 * + : Augmente le nombre de temps par mesure (maximum 7)
 * - : Diminue le nombre de temps par mesure (minimum 2)
 * n'importe quel nombre : tempo compris entre 10 et 260
 * 
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public class IHMImpl extends Observable  implements IHM , Runnable {
	
	private Controleur controleur;
	private Horloge horloge;
	private boolean keepRunning = true;
	private TOUCHE derniereToucheEnfoncee;
	private int tempoIHM;
	
	
	/**
	 * Constructeur de l'IHMImpl prend en parametre le controleur
	 * @param controleur
	 */
	public IHMImpl(Controleur controleur) {
		super();
		this.controleur = controleur;
		this.horloge = new HorlogeImpl();
		this.addObserver(this.controleur);
	}

	@Override
	public void allumerLed(LED led) {
		switch (led) {
		case LED_TEMPS:
			System.out.println("    TOC");
			break;

		case LED_MESURE:
			System.out.println("TIC");
			break;
			
		default:
			break;
		}
	}

	@Override
	public void run() {
		Scanner sc = new Scanner (System.in);
		
		System.out.println("IHM demarree");
        while (keepRunning) {
            System.out.println("Liste de commandes : ");
            System.out.println("+ : Augmente le nombre de temps par mesure (maximum 7)");
            System.out.println("- : Diminue le nombre de temps par mesure (minimum 2)");
            System.out.println("entrez un nombre : tempo compris entre 10 et 260");


            while(sc.hasNext()) {
                String s1 = sc.next();
                if(s1.equals("exit")) {
                    break;
                }
                
                if(s1.equals("start")) {
                	derniereToucheEnfoncee = TOUCHE.DEMARRER;
                	this.setChanged();
            		this.notifyObservers();
                }
                
                if(s1.equals("stop")) {
                	derniereToucheEnfoncee = TOUCHE.ARRETER;
                	this.setChanged();
            		this.notifyObservers();
                }
                
                if(s1.equals("+")) {
                	derniereToucheEnfoncee = TOUCHE.INC;
                	this.setChanged();
            		this.notifyObservers();
                }
                
                if(s1.equals("-")) {
                	derniereToucheEnfoncee = TOUCHE.DEC;
                	this.setChanged();
            		this.notifyObservers();
                }
                
                if(this.isInteger(s1) == true) {
                	int tempo = Integer.parseInt(s1);
                	if(tempo >= 20 && tempo <= 260) {
                		tempoIHM = tempo;
                		derniereToucheEnfoncee = TOUCHE.AUCUNE;
                		this.setChanged();
                		this.notifyObservers();
                	} else {
                		System.out.println("Tempo invalide");
                	}
                }
                
            }
        }
        System.out.println("Done looping.");
	}
	
	/**
	 * Determine si la chaine s est de type integer
	 * @param s
	 * @return
	 */
	private  boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    // only got here if we didn't return false
	    return true;
	}

	@Override
	public TOUCHE getToucheEnfoncee() {
		return derniereToucheEnfoncee;
	}

	@Override
	public int getTempoIHM() {
		return tempoIHM;
	}

	@Override
	public Horloge getHorloge() {
		return this.horloge;
	}
	

}
