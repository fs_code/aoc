package moteur;

import java.util.Observable;

import commandes.Commande;
import commandes.CommandeMarquerMesure;
import commandes.CommandeMarquerTemps;
import controller.Controleur;

/**
 * Classe qui implement MoteurMetronome
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public class MoteurMetronomeImpl extends Observable implements MoteurMetronome {
	
	private Controleur controleur;
	
	private float tempo;
	private int nbTempsMesure;
	private int beat;
	private boolean etatMarche;
	
	/**
	 * Constructeur du moteur métronome prend en parametre un controleur
	 * @param controleur 
	 */
	public MoteurMetronomeImpl(Controleur controleur) {
		super();
		this.controleur = controleur;
		this.addObserver(this.controleur);
		tempo = 120;
		beat = 0;
		nbTempsMesure = 4;
		etatMarche = false;
	}

	@Override
	public void click() {
		if(beat == 0) {
			Commande commandeMarquerMesure = new CommandeMarquerMesure(controleur);
			commandeMarquerMesure.execute();
		} else {
			CommandeMarquerTemps commandeMarquerTemps = new CommandeMarquerTemps(controleur);
			commandeMarquerTemps.execute();
		}

		beat++;
		if((beat % nbTempsMesure) == 0) {
			beat = 0;
		}
	}

	@Override
	public float getTempo() {
		return tempo;
	}

	@Override
	public void setTempo(float tempo) {
		if(tempo < 0) {
			return;
		}
		
		if (tempo != this.tempo) {
			this.tempo = tempo;
			
			this.setChanged();
			this.notifyObservers();
		}
	}

	@Override
	public boolean getEtatMarche() {
		return etatMarche;
	}

	@Override
	public void setEtatMarche(boolean etat) {
		etatMarche = etat;
		
		if(etat == false) {
			beat = 0;
		}
	}

	@Override
	public void setNbTemps(int nbTemps) {
		if(nbTemps <2 || nbTemps >7){
			return;
		}
		
		if(nbTempsMesure != nbTemps) {
			nbTempsMesure = nbTemps;
			
			//On redemarre au debut de la mesure
			beat = 0;
		}
	}

	@Override
	public int getNbTemps() {
		return nbTempsMesure;
	}

}
