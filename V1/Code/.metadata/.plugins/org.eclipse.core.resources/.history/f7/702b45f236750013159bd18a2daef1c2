package moteur;

/**
 * Interface MoteurMetronome
 * moteur de l'application
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public interface MoteurMetronome {

	/**
	 * Methode appelee par la commande CommandeClick (invoquee par l'horloge)
	 * invoque les commandes marquer temps / marquer mesures en fonction
	 */
	public void click();
	
	public void demarrer();
	public void arreter();
	
	/**
	 * Augmente le nombre de temps par mesure de 1 (max 7)
	 */
	public void incrementer();
	/**
	 * Diminue le nombre de temps par mesure de 1 (min 2)
	 */
	public void decrementer();
	
	/**
	 * Recupere le tempo
	 * @return float : valeur du tempo
	 */
	public float getTempo();
	/**
	 * Setter du tempo
	 * @param tempo : float (valeur du tempo)
	 */
	public void setTempo(float tempo);
	
	/**
	 * Recupere l'etat de fonctionnement du metronome (allume ou eteint) 
	 * @return : boolean --> true est en marche ; false --> est eteint
	 */
	public boolean getEtatMarche();
	/**
	 * Setter de l'etat de marche du metronome
	 * @param etat : boolean --> true marche ; false --> arret
	 */
	public void setEtatMarche(boolean etat);
	
	/**
	 * Regle le nombre de temps
	 * @param nbTemps
	 */
	public void setNbTemps(int nbTemps);
	public int getNbTemps();
	
}
