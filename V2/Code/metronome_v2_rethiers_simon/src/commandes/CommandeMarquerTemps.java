package commandes;

import controller.Controleur;


/**
 * Classe implémentant l'interface Commande
 * envoie une commande pour marquer le temps
 * 
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public class CommandeMarquerTemps implements Commande {
	private Controleur controleur;
	@Override
	public void execute() {
		controleur.marquerTemps();
	}
	
	/**
	 * Constructeur
	 * @param controleur classe concrete implementant l'interface Controleur
	 */
	public CommandeMarquerTemps(Controleur controleur) {
		super();
		this.controleur = controleur;
	}

}
