package commandes;

import controller.Controleur;

/**
 * Classe implémentant l'interface Commande
 * envoie une commande pour marquer la mesure
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public class CommandeMarquerMesure implements Commande {

	private Controleur controleur;
	
	@Override
	public void execute() {
		controleur.marquerMesure();
	}

	/**
	 * Constructeur
	 * @param controleur classe concrete implementant l'interface Controleur
	 */
	public CommandeMarquerMesure(Controleur controleur) {
		super();
		this.controleur = controleur;
	}
	
}
