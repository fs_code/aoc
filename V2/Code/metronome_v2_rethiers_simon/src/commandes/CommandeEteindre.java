package commandes;

import ihm.Afficheur;
import ihm.Afficheur.LED;

/**
 * Classe concrete permettant d'eteindre une led
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public class CommandeEteindre implements Commande {
	
	private Afficheur afficheur;
	private LED ledType;

	/**
	 * Constructeur de la classe
	 * prend en parametre l'afficheur (pour appeler la methode eteindre) et le type de la led
	 * @param a
	 * @param ledType
	 */
	public CommandeEteindre(Afficheur a , LED ledType) {
		super();
		afficheur = a;
		this.ledType = ledType;
	}

	@Override
	public void execute() {
		afficheur.eteindreLED(ledType);
	}

}
