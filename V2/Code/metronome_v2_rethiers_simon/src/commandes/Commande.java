package commandes;

/**
 * 
 * @author Jean-Francois Rethiers & Flavien Simon
 * Interface commande (pattern Command)
 */
public interface Commande {
	/**
	 * Methode execute du pattern Command
	 */
	public void execute();
}
