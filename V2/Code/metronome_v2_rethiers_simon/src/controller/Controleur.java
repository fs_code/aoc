package controller;

import java.util.Observer;

/**
 * 
 * @author Jean-Francois Rethiers & Flavien Simon
 * Interface Controleur
 * Fait le lien entre le moteur metronome et l'IHM
 */
public interface Controleur extends Observer{
	/**
	 * D�marre le metronome
	 */
	public void demarrer();
	/**
	 * Arrete le metronome
	 */
	public void arreter();
	/**
	 * Marque le temps (appele par l'horloge via la commande MarquerMesure pour le moteur)
	 */
	public void marquerTemps();
	/**
	 * Marque la mesure (appele par l'horloge via la commande MarquerTemps pour le moteur)
	 */
	public void marquerMesure();
	/**
	 * Augmente le nombre de temps par mesure (de 1) du moteur
	 * Valeur maximale : 7
	 */
	public void incrementerNBTempsMesure();
	/**
	 * Diminue le nombre de temps par mesure (de 1) du moteur
	 * Valeur minimale : 2
	 */
	public void decrementerNBTempsMesure();
}
