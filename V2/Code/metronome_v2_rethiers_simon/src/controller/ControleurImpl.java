package controller;


import ihm.Afficheur.LED;
import ihm.Clavier.TOUCHE;
import ihm.EmetteurSonore.CLIC;
import ihm.Materiel;
import ihm.MaterielImpl;
import ihm.MoletteImpl;

import java.util.Observable;

import commandes.Commande;
import commandes.CommandeClick;
import moteur.MoteurMetronome;
import moteur.MoteurMetronomeImpl;

/**
 * Classe implementant l'interface Controleur
 * possède une reference vers 
 * un MoteurMetronome
 * une Commande
 * une IHM
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public class ControleurImpl implements Controleur {

	private MoteurMetronome moteurMetronome;
	private Commande commandeClick;
	private Materiel materiel;
	
	private int tempoMoteurMetronome;

	/**
	 * Constructeur
	 * initialise le moteur l'IHM et la commande click
	 */
	public ControleurImpl() {
		super();
		moteurMetronome = new MoteurMetronomeImpl(this);		
		materiel = new MaterielImpl(this);
		commandeClick = new CommandeClick(moteurMetronome);
		
		tempoMoteurMetronome = this.getTempoFromMolette();
		moteurMetronome.setTempo(tempoMoteurMetronome);
		this.afficherTempo();
	}

	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof Materiel){	
			int tempoMateriel = this.getTempoFromMolette();
			
			if(tempoMateriel != tempoMoteurMetronome) {
				tempoMoteurMetronome = tempoMateriel;
				moteurMetronome.setTempo(tempoMoteurMetronome);
			}
			else { //un bouton a ete presse
				if(materiel.getClavier().touchePressee(TOUCHE.DEMARRER))
					this.demarrer();
				
				if(materiel.getClavier().touchePressee(TOUCHE.ARRETER))
					this.arreter();
				
				if(materiel.getClavier().touchePressee(TOUCHE.INC))
					this.incrementerNBTempsMesure();
				
				if(materiel.getClavier().touchePressee(TOUCHE.DEC))
					this.decrementerNBTempsMesure();

			}

		}

		if(o instanceof MoteurMetronome){	
			
			this.afficherTempo();
			
			if(moteurMetronome.getEtatMarche() == true){
				//Changement de tempo
				materiel.getHorloge().desactiver();
				materiel.getHorloge().activerPeriodiquement(commandeClick, this.tempoEnSeconde());
			}
		}

	}
	
	/**
	 * Renvoi le tempo de la molette
	 * @return
	 */
	private int getTempoFromMolette() {
		float positionMolette = materiel.getMolette().position();
		MoletteImpl mol = (MoletteImpl)this.materiel.getMolette();
		return (int) (mol.getMaxTempo() * positionMolette);
	}
	
	/**
	 * Affiche le tempo du moteur dans l'IHM
	 */
	private void afficherTempo() {
		int tempo = moteurMetronome.getTempo();
		materiel.getAfficheur().afficherTempo(tempo);
	}

	@Override
	public void demarrer() {
		System.out.println("Metronome demarrer");

		
		if(moteurMetronome.getEtatMarche() == true) {
			return;
		}

		moteurMetronome.setEtatMarche(true);
		materiel.getHorloge().activerPeriodiquement(commandeClick, this.tempoEnSeconde());
	}
	/**
	 * Donne la valeur du tempo actuel en seconde
	 * @return
	 */
	private float tempoEnSeconde() {
		float tempo = moteurMetronome.getTempo();
		float nbSecPerMinute = 60;
		return nbSecPerMinute/tempo;
	}

	@Override
	public void arreter() {
		System.out.println("Metronome arreter");
		
		if(moteurMetronome.getEtatMarche() == false)
			return;

		moteurMetronome.setEtatMarche(false);
		materiel.getHorloge().desactiver();
	}

	@Override
	public void marquerTemps() {
		System.out.println("  toc");

		materiel.getAfficheur().allumerLED(LED.LED_TEMPS);
		materiel.getEmetteurSonore().emettreClic(CLIC.TEMPS);
	}

	@Override
	public void marquerMesure() {
		System.out.println("tic");

		materiel.getAfficheur().allumerLED(LED.LED_MESURE);
		materiel.getEmetteurSonore().emettreClic(CLIC.MESURE);
	}

	@Override
	public void incrementerNBTempsMesure() {
		int nbTempsMesure = moteurMetronome.getNbTemps();
		if(nbTempsMesure < 8) {
			nbTempsMesure++;
			moteurMetronome.setNbTemps(nbTempsMesure);
		}
		System.out.println("nb temps Mesure = "+nbTempsMesure);
	}

	@Override
	public void decrementerNBTempsMesure() {
		int nbTempsMesure = moteurMetronome.getNbTemps();
		if(nbTempsMesure > 2) {
			nbTempsMesure--;
			moteurMetronome.setNbTemps(nbTempsMesure);
		}
		System.out.println("nb temps Mesure = "+nbTempsMesure);
	}

}
