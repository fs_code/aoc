package main;

import controller.Controleur;
import controller.ControleurImpl;

/**
 * Classe de demarrage de l'application
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public class Main {

	/**
	 * Methode main de l'application
	 * @param args
	 */
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Controleur controleur = new ControleurImpl();
	}
	
}
