package horloge;

import java.util.Timer;
import java.util.TimerTask;

import commandes.Commande;

/**
 * Classe de type TimerTask
 * possede un timer qui permet de s'auto annuler une fois que la commande est executee
 * 
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public class TachePeriodiqueDelai extends TimerTask{

	private Timer timer;
	private Commande cmd;
	
	/**
	 * Constructeur de la classe, prend en parametre :
	 * une commande a executer
	 * son timer 
	 * @param cmd
	 * @param timer
	 */
	public TachePeriodiqueDelai(Commande cmd , Timer timer) {
		this.cmd = cmd;
		this.timer = timer;
	}

	/**
	 * Methode run
	 * execute la commande et annule le timer
	 */
	public void run() {
		this.cmd.execute();
		timer.cancel();
	}
}
