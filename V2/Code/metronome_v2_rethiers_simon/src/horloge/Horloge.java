package horloge;

import commandes.Commande;

/**
 * Interface de l'horloge
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public interface Horloge {

	/**
	 * Appel periodique de l’opération execute() de cmd toutes les periodeEnSecondes secondes une précision d’une milliseconde.
	 * @param cmd Commande a appeler
	 * @param periodeEnSecondes Valeur de la periode
	 */
	public void activerPeriodiquement(Commande cmd,	float periodeEnSecondes) ;
	
	/**
	 * Appel de l’operation execute() de cmd apres un delai de delaiEnSecondes secondes avec une precision d’une milliseconde.
	 * @param cmd Commande a appeler
	 * @param delaiEnSecondes
	 */
	public void activerApresDelai(Commande cmd, float delaiEnSecondes) ;
	
	/**
	 * Permet de desactiver l'horloge
	 * @param cmd commande a appeler
	 */
	public void desactiver();
	
}
