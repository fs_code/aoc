package horloge;

import java.util.TimerTask;

import commandes.Commande;

/**
 * Classe permetant d'exectuer une commande periodiquement
 * 
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public class TachePeriodique extends TimerTask {
	
	private Commande cmd;

	@Override
	public void run() {
		cmd.execute();
	}

	/**
	 * constructeur
	 * prend en parametre une commande qui s'executera dans la m�thode run()
	 * @param cmd
	 */
	public TachePeriodique(Commande cmd) {
		super();
		this.cmd = cmd;
	}
	
}
