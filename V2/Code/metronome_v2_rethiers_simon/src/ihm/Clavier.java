package ihm;

/**
 * Interface designant le clavier
 * contient plusieurs touches designees par le type enum TOUCHE
 * DEMARRER pour demarrer le metronome
 * ARRETER pour arreter le metronome
 * INC pour augmenter de 1 en 1 le nombre de temps par mesure (max 7)
 * DEC pour diminuer de 1 en 1 le nombre de temps par mesure (min 2)
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public interface Clavier {

	public enum TOUCHE {DEMARRER, ARRETER, INC, DEC };
	
	/**
	 * Return if the Touche is pressed
	 * @param i
	 * @return
	 */
	boolean touchePressee(TOUCHE i) ;
	
}
