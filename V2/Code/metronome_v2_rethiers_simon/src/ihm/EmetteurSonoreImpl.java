package ihm;

/**
 * Classe concrete implementant l'interface EmetteurSonore
 * classe MakeSound utilisee pour jouer plusieurs sons
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public class EmetteurSonoreImpl implements EmetteurSonore {
	
	private MakeSound ms;
	
	/**
	 * Constructeur de la classe
	 */
	public EmetteurSonoreImpl() {
		ms = new MakeSound();
	}

	@Override
	public void emettreClic(CLIC clic) {
		switch (clic) {
		case MESURE:
			ms.playSound("HighTone.wav");
			break;

		case TEMPS:
			ms.playSound("LowTone.wav");
		break;

		default:
			break;
		}
	}
	
}
