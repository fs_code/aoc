package ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Hashtable;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Classe implementant l'interface Molette
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public class MoletteImpl extends JPanel implements Molette, ChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Materiel materiel;
	
	private int max_tempo;
	private int min_tempo;
	private JSlider molette;
	private float position;
	
	/**
	 * Consctructeur de la classe
	 * @param materiel
	 */
	public MoletteImpl(Materiel materiel) {
		super();
		this.materiel = materiel;
		
		max_tempo = 260;
		min_tempo = 20;
		position = 120;
		
		this.setLayout(new BorderLayout());
		
		molette = new JSlider(JSlider.HORIZONTAL, min_tempo, max_tempo, (int)position);
		molette.addChangeListener(this);
		molette.setPreferredSize(new Dimension(150,50));
		
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put( new Integer( 0 ), new JLabel("Stop") );
		
		for(int i = min_tempo ; i <= max_tempo ; i+=20) {
			labelTable.put( new Integer( i), new JLabel(""+i) );
		}
		
		labelTable.put( new Integer( max_tempo ), new JLabel(""+max_tempo) );
		molette.setLabelTable( labelTable );
		molette.setPaintLabels(true);
		molette.setMajorTickSpacing(10);
		molette.setPaintTicks(true);
		molette.setPaintLabels(true);
		
		this.add(molette, BorderLayout.SOUTH);
		
	}

	@Override
	public float position() {
		return position/max_tempo;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider)e.getSource();
	    if (!source.getValueIsAdjusting()) {
	        position = (int)source.getValue();
	    }
	    this.materiel.changerEtat();
	}
	
	public int getMaxTempo() {
		return max_tempo;	
	}
	
	public int getMinTempo() {
		return min_tempo;	
	}

}
