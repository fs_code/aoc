package ihm;

import horloge.Horloge;

/**
 * Designe l'interface a implementer pour representer le materiel
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public interface Materiel {
	
	/**
	 * Retourne l'horloge
	 * @return
	 */
	public Horloge getHorloge();
	
	/**
	 * Retourne le clavier
	 * @return
	 */
	public Clavier getClavier();
	
	/**
	 * Retourne la molette
	 * @return
	 */
	public Molette getMolette();
	
	/**
	 * Retourne l'emetteur sonore
	 * @return
	 */
	public EmetteurSonore getEmetteurSonore();
	
	/**
	 * Retourne l'afficheur
	 * @return
	 */
	public Afficheur getAfficheur();

	/**
	 * Permet de changer l'etat de l'objet pour notifier ses observateurs
	 */
	public void changerEtat();
	
}
