package ihm;

/**
 * Interface designant le molette qui permet de regler le tempo
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public interface Molette {

	/**
	 * Retourne la position de la molette, entre 0.0 et 1.0
	 * @return
	 */
	public float position();
	
}
