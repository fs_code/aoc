package ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * JPanel qui affiche le tempo dans un Label
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public class Ecran extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel label;
	
	/**
	 * Constructeur de l'�cran
	 */
	public Ecran() {
		super();
		Color bgColor = new Color(91, 103, 127);
		this.setBackground(bgColor);
		
		int width = 500;
		int height = 200;
		
		this.setPreferredSize(new Dimension(width,height));
		
		label = new JLabel();
		label.setPreferredSize(new Dimension(width - 50,height - 10));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont (label.getFont ().deriveFont (64.0f));
		label.setOpaque(false);
		
		this.add((Component) label, BorderLayout.CENTER);

	}

	/**
	 * Commande qui affiche le tempo dans le Label
	 * @param valeurTempo
	 */
	public void afficherTempo(int valeurTempo) {
		label.setText("<html><font color='white'>"+valeurTempo+"</font></html>");
	}

}
