package ihm;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import commandes.Commande;
import commandes.CommandeEteindre;

/**
 * Classe concrete implementant l'interface Afficheur
 * JPanel qui contient les deux leds et l'�cran d'affichage
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public class AfficheurImpl extends JPanel implements Afficheur {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Materiel materiel;
	
	private Led ledMesure;
	private Led ledTemps;
	
	private Ecran ecran;

	/**
	 * Constructeur de la classe, prend en parametre un objet Materiel pour le notifier
	 * @param materiel
	 */
	public AfficheurImpl(Materiel materiel) {
		super();
		this.materiel = materiel;
		JPanel LEDPanel = new JPanel();
		
		LEDPanel.setLayout(new BorderLayout());

		ledMesure = new Led();
		ledTemps = new Led();

		LEDPanel.add(ledMesure,BorderLayout.WEST);
		LEDPanel.add(ledTemps,BorderLayout.EAST);
		
		this.add(LEDPanel,BorderLayout.WEST);
		
		ecran = new Ecran();
		this.add(ecran,BorderLayout.SOUTH);
	}

	@Override
	public void allumerLED(LED ledType) {
		
		switch (ledType) {
		case LED_MESURE:
			ledMesure.allumer();
			break;
			
		case LED_TEMPS:
			ledTemps.allumer();
			break;

		default:
			break;
		}
		
		Commande cmdEteindre = new CommandeEteindre(this, ledType);
		float delaiEteindre = (float) 0.10;
		materiel.getHorloge().activerApresDelai(cmdEteindre, delaiEteindre);
		
	}

	
	
	@Override
	public void eteindreLED(LED ledType) {
		switch (ledType) {
		case LED_MESURE:
			ledMesure.eteindre();
			break;
			
		case LED_TEMPS:
			ledTemps.eteindre();
			break;

		default:
			break;
		}

	}

	@Override
	public void afficherTempo(int valeurTempo) {
		this.ecran.afficherTempo(valeurTempo);
	}

}
