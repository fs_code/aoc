package ihm;

/**
 * Emet des sons pour marquer les battements du metronome
 * MESURE est un clic plus aigu � chaque debut de mesure
 * TEMPS est un clic a chaque battement
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public interface EmetteurSonore {
	
	public enum CLIC {MESURE, TEMPS};

	
	/**
	 * Emet le clic du tempo
	 * @return
	 */
	public void emettreClic(CLIC clic);

}
