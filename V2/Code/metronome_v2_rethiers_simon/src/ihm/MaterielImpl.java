package ihm;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Observable;

import javax.swing.JFrame;

import controller.Controleur;
import horloge.Horloge;
import horloge.HorlogeImpl;

/**
 * Classe implementant l'interface Materiel
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public class MaterielImpl extends Observable implements Materiel {
	Controleur controleur;
	
	private Horloge horloge;
	private Clavier clavier;
	private EmetteurSonore emetteurSonore;
	private Molette molette;
	private Afficheur afficheur;
	
	private JFrame applicationFrame;
	
	/**
	 * constructeur de la classe
	 * @param controleur
	 */
	public MaterielImpl(Controleur controleur) {
		super();
		this.controleur = controleur;
		
		this.addObserver(controleur);
		
		horloge = new HorlogeImpl();
		clavier = new ClavierImpl(this);
		emetteurSonore = new EmetteurSonoreImpl();
		molette = new MoletteImpl(this);
		afficheur = new AfficheurImpl(this);
		
		this.initApplicationUI();
	}

	@Override
	public Horloge getHorloge() {
		return horloge;
	}

	@Override
	public Clavier getClavier() {
		return clavier;
	}

	@Override
	public Molette getMolette() {
		return molette;
	}

	@Override
	public EmetteurSonore getEmetteurSonore() {
		return emetteurSonore;
	}

	@Override
	public Afficheur getAfficheur() {
		return afficheur;
	}

	/**
	 * Initialise l'interface
	 */
	private void initApplicationUI() {
		this.applicationFrame = new JFrame();
		
		int widthFrame = 600;
		int heightFrame = 300;
		
		this.applicationFrame.setSize(widthFrame, heightFrame);
		this.applicationFrame.setTitle("Projet Metronome");
		this.applicationFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.applicationFrame.setLayout(new BorderLayout());
		
		this.applicationFrame.getContentPane().add((Component) afficheur, BorderLayout.CENTER);
		this.applicationFrame.getContentPane().add((Component) clavier, BorderLayout.SOUTH);
		this.applicationFrame.getContentPane().add((Component) molette, BorderLayout.NORTH);

		this.applicationFrame.setVisible(true);
	}

	@Override
	public void changerEtat() {
		this.setChanged();
		this.notifyObservers();
	}
	
}
