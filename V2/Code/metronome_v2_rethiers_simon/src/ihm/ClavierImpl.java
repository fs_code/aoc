package ihm;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Classe concrete implementant l'interface Clavier
 * JPanel qui contient les touches du clavier
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public class ClavierImpl extends JPanel implements Clavier,ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Materiel materiel;
	private Map<TOUCHE, Boolean> touches;
	
	private JButton boutonDemarrer;
	private JButton boutonArreter;
	private JButton boutonDecrementer;
	private JButton boutonIncrementer;

	/**
	 * Constructeur de la classe, prend en parametre un objet Materiel pour le notifier
	 * @param materiel
	 */
	public ClavierImpl(Materiel materiel) {
		super();
		this.materiel = materiel;
		this.initBoutons();
	}
	
	/**
	 * Initialise les touches et boutons de l'interface
	 */
	private void initBoutons() {
		touches = new HashMap<TOUCHE, Boolean>();
		touches.put(TOUCHE.DEMARRER, false);
		touches.put(TOUCHE.ARRETER, false);
		touches.put(TOUCHE.DEC, false);
		touches.put(TOUCHE.INC, false);

		
		boutonDemarrer 	= new JButton();
		boutonArreter 	= new JButton();
		boutonDecrementer 	= new JButton();
		boutonIncrementer 	= new JButton();


		boutonDemarrer.setBackground(new Color(0, 178, 51));
		boutonDemarrer.setBorderPainted(false);
		boutonDemarrer.setOpaque(true);
		boutonArreter.setBackground(new Color(255, 0, 0));
		boutonArreter.setOpaque(true);
		boutonArreter.setBorderPainted(false);
		boutonDecrementer.setText("-");
		boutonIncrementer.setText("+");

		boutonDemarrer.addActionListener(this);
		boutonArreter.addActionListener(this);
		boutonDecrementer.addActionListener(this);
		boutonIncrementer.addActionListener(this);
		
		this.add(boutonDemarrer);
		this.add(boutonArreter);
		this.add(boutonDecrementer);
		this.add(boutonIncrementer);
	}

	@Override
	public boolean touchePressee(TOUCHE i) {
		boolean estPressee = touches.get(i);
		touches.put(i, false);
		return estPressee;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		Object source = e.getSource();
		
		if(source == boutonDemarrer) {
			touches.put(TOUCHE.DEMARRER, true);
		}
		
		if(source == boutonArreter) {
			touches.put(TOUCHE.ARRETER, true);
		}
		
		if(source == boutonDecrementer) {
			touches.put(TOUCHE.DEC, true);
		}
		
		if(source == boutonIncrementer) {
			touches.put(TOUCHE.INC, true);
		}
	
		materiel.changerEtat();		
	}

}
