package ihm;

/**
 * Interface qui defini les methodes a implementer pour realiser l'afficheur
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public interface Afficheur {
	
	public enum LED {LED_TEMPS , LED_MESURE}

	/**
	 * Allume la LED designee par son type
	 * @param ledType
	 */
	void allumerLED(LED ledType);
	
	/**
	 * Eteint la LED designee par son type
	 * @param ledType
	 */
	void eteindreLED(LED ledType);
	
	/**
	 * Affiche un entier sur l'afficheur externe du metronome
	 * @param valeurTempo
	 */
	void afficherTempo(int valeurTempo);

}
