package ihm;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * JPanel modelisant une led
 * peut etre eteinte ou allumee
 * @author Jean Francois Rethiers & Flavien Simon
 *
 */
public class Led extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Color off;
	private Color on;
	
	/**
	 * Constructeur de la LED
	 */
	public Led() {
		off = Color.BLACK;
		on = new Color(255,25,25);
		this.setPreferredSize(new Dimension(30,30));
		this.setBorder(new EmptyBorder(10, 10, 10, 10) );
		this.eteindre();
	}
	
	/**
	 * Allume la led
	 */
	public void allumer() {
		this.setBackground(on);
	}
	
	/**
	 * Eteint la led
	 */
	public void eteindre() {
		this.setBackground(off);
	}
}
