package moteur;

/**
 * Interface MoteurMetronome
 * moteur de l'application
 * @author Jean-Francois Rethiers & Flavien Simon
 *
 */
public interface MoteurMetronome {

	/**
	 * Methode appelee par la commande CommandeClick (invoquee par l'horloge)
	 * invoque les commandes marquer temps / marquer mesures en fonction
	 */
	public void click();
	
	/**
	 * Recupere le tempo
	 * @return float : valeur du tempo
	 */
	public int getTempo();
	/**
	 * Setter du tempo
	 * @param tempo : float (valeur du tempo)
	 */
	public void setTempo(int tempo);
	
	/**
	 * Recupere l'etat de fonctionnement du metronome (allume ou eteint) 
	 * @return : boolean --> true est en marche ; false --> est eteint
	 */
	public boolean getEtatMarche();
	/**
	 * Setter de l'etat de marche du metronome
	 * @param etat : boolean --> true marche ; false --> arret
	 */
	public void setEtatMarche(boolean etat);
	
	/**
	 * Regle le nombre de temps par mesure
	 * @param nbTemps nouveau nombre de temps par mesure
	 */
	public void setNbTemps(int nbTemps);
	
	/**
	 * Recupere le nombre de temps par mesure
	 * @return int : nombre de temps par mesure
	 */
	public int getNbTemps();
	
}
